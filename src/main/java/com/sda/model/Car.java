package com.sda.model;

import java.util.Random;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

@Component
public class Car implements InitializingBean, DisposableBean {

	private int id;
	private String brand;
	private String model;
	private Engine engine;
	
	private static final Random random = new Random();
	
	public Car() {
		// this.id = (int) (Math.random() * 50);
		// setId((int) (Math.random() * 50));
		setId(random.nextInt(50) + 1);
	}
	
	public Car(String brand, String model, Engine engine) {
		this();
		this.brand = brand;
		this.model = model;
		this.engine = engine;
	}

	public Engine getEngine() {
		return engine;
	}

	public void setEngine(Engine engine) {
		this.engine = engine;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}

	@Override
	public String toString() {
		return "Car [id=" + id + ", brand=" + brand + ", model=" + model + ", engine=" + engine + "]";
	}

	public void destroy() throws Exception {
//		System.out.println("Bean destroy");
	}

	public void afterPropertiesSet() throws Exception {
//		System.out.println("After properties set");
	}
	
}
