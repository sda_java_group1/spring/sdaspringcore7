package com.sda.main;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import com.sda.model.Car;
import com.sda.model.Engine;

@Configuration
public class AppConfig {
	
	@Bean
	@Scope("prototype")
	public Car randomCar() {
		return new Car();
	}
	
	@Bean
	public Car carBMW() {
		return new Car("BMW", "3-Series", line4Engine());
	}
	
	@Bean
	public Car carMercedes() {
		return new Car("Mercedes", "ML", v6Engine());
	}
	
	@Bean
	public Engine line4Engine() {
		Engine engine = new Engine();
		engine.setType("Line-4");
		return engine;
	}
	
	@Bean
	public Engine v6Engine() {
		Engine engine = new Engine();
		engine.setType("V6");
		return engine;
	}

}
