package com.sda.main;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;

import com.sda.model.Car;

@ComponentScan(basePackages = {"com.sda"})
public class App {

	public static void main(String[] args) {
		System.out.println();
		System.out.println("CLASSIC");
		System.out.println("-------");
		System.out.println();
		
		Car car1 = new Car();

		Car car2 = new Car("BMW", "X5", null);
		Car car3 = new Car("Dacia", "Logan", null);

		System.out.println(car1);
		System.out.println(car2);
		System.out.println(car3);

		System.out.println();
		System.out.println("SPRING");
		System.out.println("------");
		System.out.println();
		
		// ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("bean-configuration.xml");
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
		
		Car rCar1 = context.getBean("randomCar", Car.class);
		Car rCar2 = context.getBean("randomCar", Car.class);
		Car rCar3 = context.getBean("randomCar", Car.class);
		System.out.println(rCar1);
		System.out.println(rCar2);
		System.out.println(rCar3);
		
		Car carBMW = context.getBean("carBMW", Car.class);
		Car carMercedes = context.getBean("carMercedes", Car.class);
		System.out.println(carBMW);
		System.out.println(carMercedes);
		
		context.close();
	}

}
